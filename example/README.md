# Wire Hobo example server

[![Docker Repository on Quay](https://quay.io/repository/elmiko/wire-hobo-example-server/status "Docker Repository on Quay")](https://quay.io/repository/elmiko/wire-hobo-example-server)

The files in this folder are for building a development server to use as an
example. The `Dockerfile` can be used to generate a new image and for
learning, and it will be automatically built and available from
`quay.io/elmiko/wire-hobo-example-server`.

The example server contains a preconfigured settings file and a database
loaded with a few sample entries.

## How to operate the example server

Start a server.
```
podman run --rm -it -p 8080:8080 quay.io/elmiko/wire-hobo-example-server
```

Make a call to the server.
```
curl -H "accept: application/json" \
     -H "wire-hobo-apikey: secr3t" \
     http://localhost:8080
```

