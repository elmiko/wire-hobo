# the wire hobo

[![Gitlab Pipeline Status](https://gitlab.com/elmiko/wire-hobo-server/badges/master/pipeline.svg)](https://gitlab.com/elmiko/wire-hobo-server/-/commits/master)
[![Docker Repository on Quay](https://quay.io/repository/elmiko/wire-hobo-server/status "Docker Repository on Quay")](https://quay.io/repository/elmiko/wire-hobo-server)

The wire hobo keeps track of all the places you care to tell it about. It
rides the information super highway with you, marking the trails you've trod
with signs of remembrance.

# quickstart

see the [example server](/example) for a simplified way to get started
playing with the wire-hobo-server.

# setup for local development

if you would like to start hacking on the wire hobo, please see the
[developer documentation](/docs/developer.md).

# how to communicate with server

use the REST API defined in [docs/openapi.yaml](docs/openapi.yaml). you must
set the request header `wire-hobo-apikey` to the api key you configured when
you deployed the server. this will be clear text, so make sure you are either
using transport layer security, on a private network, or don't care about
someone sniffing your key. this server is designed as a single key appliance.
