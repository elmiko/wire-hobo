# -*- coding: utf-8 -*-
from datetime import datetime, timedelta
from django.contrib.syndication.views import Feed
from trails.models import CampModel


class TodaysCampsFeed(Feed):
    title = "Latest Wire Hobo camps"
    link = "/"
    description = "Camps visited in the last 24 hours."

    def items(self):
        td = datetime.now() - timedelta(days=1)
        return CampModel.objects.filter(modified__gt=td).order_by('-modified')

    def item_title(self, item):
        return item.title

    def item_description(self, item):
        ret = ''
        signs = item.signs.all()
        if len(signs) > 0:
            ret = signs[0].name
            for s in signs[1:]:
                ret = f'{ret}, {s.name}'
        return ret

    def item_guid(self, item):
        return item.id

    def item_link(self, item):
        return item.url
