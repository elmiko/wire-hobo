# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from datetime import datetime
from datetime import timedelta

from django.test import Client, TestCase
import feedparser

from .models import CampModel
from .views import TrailsInfo


class TrailsFeedTestCase(TestCase):
    fixtures = ['exampledb.json']

    def setUp(self):
        self.c = Client()

    def test_nothing_in_last_day(self):
        res = self.c.get('/feed')
        data = feedparser.parse(res.content.decode('utf-8'))
        self.assertEqual(len(data.entries), 0)

    def test_one_item(self):
        title = 'title1'
        url = 'http://url1.test'
        CampModel.objects.create(title=title, url=url)
        res = self.c.get('/feed')
        data = feedparser.parse(res.content.decode('utf-8'))
        self.assertEqual(len(data.entries), 1)
        entry = data.entries[0]
        self.assertEqual(entry.title, title)
        self.assertEqual(entry.link, url)


class TrailsViewTestCase(TestCase):

    def test_camp_to_dict_created_today(self):
        """
        camps created today should show "today" in console.
        """
        c = CampModel(title='test', url='http://test.test')
        cd = TrailsInfo.camp_to_dict(c)
        self.assertEqual(cd['created'], 'today')

    def test_camp_to_dict_created_before_today(self):
        """
        camps created before today should show "{day} {month} {year}" in console.
        """
        t = datetime.now() - timedelta(days=2)
        c = CampModel(title='test', url='http://test.test', created=t)
        cd = TrailsInfo.camp_to_dict(c)
        ot = datetime.strptime(cd['created'], '%d %B %Y')
        self.assertEqual(ot.day, t.day)
        self.assertEqual(ot.month, t.month)
        self.assertEqual(ot.year, t.year)

