from rest_framework import serializers
from trails import models


class SignSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.SignModel
        fields = ('id', 'name')


class CampSerializer(serializers.ModelSerializer):
    signs = serializers.SlugRelatedField(
        many=True,
        slug_field='name',
        required=False,
        queryset=models.SignModel.objects.all())

    class Meta:
        model = models.CampModel
        fields = ('id', 'title', 'url', 'signs', 'created', 'modified')
        extra_kwargs = {
            'url': {'required': True},
            'title': {'required': True},
        }
