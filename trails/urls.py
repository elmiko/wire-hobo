from django import urls
from rest_framework import urlpatterns as up
from rest_framework import renderers
from trails import views
from trails import feeds


sign_list = views.SignViewSet.as_view({
    'get': 'list',
    'post': 'create'
})

sign_detail = views.SignViewSet.as_view({
    'get': 'retrieve',
    'put': 'update',
    'delete': 'destroy'
})

camp_list = views.CampViewSet.as_view({
    'get': 'list',
    'post': 'create'
})

camp_detail = views.CampViewSet.as_view({
    'get': 'retrieve',
    'put': 'update',
    'delete': 'destroy'
})

urlpatterns = [
    urls.re_path(r'^$',
             views.TrailsInfo.as_view(),
             name='trails-info'),
    urls.re_path(r'^signs$',
             sign_list,
             name='signs-list'),
    urls.re_path(r'^signs/(?P<pk>[0-9a-z-]{36})$',
             sign_detail,
             name='signs-detail'),
    urls.re_path(r'^camps$',
             camp_list,
             name='camps-list'),
    urls.re_path(r'^camps/(?P<pk>[0-9a-z-]{36})$',
             camp_detail,
             name='camps-detail'),
    urls.re_path(r'^feed', feeds.TodaysCampsFeed()),
]

urlpatterns = up.format_suffix_patterns(urlpatterns)
