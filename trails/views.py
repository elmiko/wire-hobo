# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from datetime import datetime

from django import shortcuts
from rest_framework import response
from rest_framework import views
from rest_framework import viewsets

from trails import models
from trails import serializers
from wire_hobo.version import VERSION


class TrailsInfo(views.APIView):
    @staticmethod
    def camp_to_dict(camp):
        created_delta = datetime.now(tz=None) - camp.created.replace(tzinfo=None)
        if created_delta.days == 0:
            created = 'today'
        else:
            created = camp.created.strftime("%d %B %Y")
        return {
            'title': camp.title,
            'url': camp.url,
            'created': created,
            'signs': [s.name for s in camp.signs.all()],
        }

    def _get_html(self, request):
        query = models.CampModel.objects.all()
        start = 0
        count = 25
        if 'count' in request.GET:
            count = request.GET.get('count')
            try:
                count = int(count)
                if count < 0:
                    count = 1
            except Exception:
                count = 25
        if 'sign' in request.GET:
            signs = request.GET.getlist('sign')
            for s in signs:
                query = query.filter(signs__name=s)
        if 'search' in request.GET:
            term = request.GET.get('search')
            query = query.filter(title__icontains=term)
        if 'start' in request.GET:
            start = request.GET.get('start')
            try:
                start = int(start)
                if start < 0:
                    start = 0
            except Exception:
                pass

        if count == 0:
            camps = [ self.camp_to_dict(c) for c in query.order_by('-created') ]

            return shortcuts.render(
                request,
                'trails/index.html',
                {'camps': camps}
            )

        camps = [ self.camp_to_dict(c) for c in query.order_by('-created')[start:start+count] ]

        idxprev = start - count
        idxprev = None if idxprev < 0 else idxprev
        idxnext = start + count
        idxnext = idxnext if idxnext < len(query) else None

        return shortcuts.render(
            request,
            'trails/index.html',
            {'camps': camps,
             'count': count,
             'idxprev': idxprev,
             'idxnext': idxnext}
        )

    def _get_json(self, request):
        camps = models.CampModel.objects.all()
        modified = ('' if len(camps) == 0
                    else camps.order_by('-modified')[0].modified)
        return response.Response({
            'server': {
                'version': VERSION,
            },
            'trails': {
                'signs': len(models.SignModel.objects.all()),
                'camps': len(camps),
                'modified': modified
            },
        })

    def get(self, request, format=None):
        accept = request.META.get('HTTP_ACCEPT', '')
        if 'application/json' in accept:
            ret = self._get_json(request)
        else:
            ret = self._get_html(request)
            ret['Strict-Transport-Security'] = 'max-age=63072000'
            ret['X-Content-Type-Options'] = 'nosniff'
            ret['Content-Security-Policy'] = "default-src https: 'unsafe-inline'; frame-ancestors 'none'"
            ret['X-Frame-Options'] = 'DENY'
        return ret


class SignViewSet(viewsets.ModelViewSet):
    queryset = models.SignModel.objects.all()
    serializer_class = serializers.SignSerializer

    @staticmethod
    def create_signs(signs):
        for sign in signs:
            models.SignModel.objects.get_or_create(name=sign)

    def list(self, request):
        count = self.request.query_params.get('count', None)
        start = self.request.query_params.get('start', None)
        if start:
            try:
                start = int(start)
                if start < 0:
                    start = 0
            except Exception:
                pass
        if count:
            try:
                count = int(count)
                if count < 0:
                    count = 0
                if start:
                    count = start + count
            except Exception:
                pass
        self.queryset = self.queryset[start:count]
        return super(SignViewSet, self).list(request)


class CampViewSet(viewsets.ModelViewSet):
    queryset = models.CampModel.objects.all()
    serializer_class = serializers.CampSerializer

    def create(self, request):
        SignViewSet.create_signs(request.data.get('signs', []))
        return super(CampViewSet, self).create(request)

    def update(self, request, pk=None):
        SignViewSet.create_signs(request.data.get('signs', []))
        return super(CampViewSet, self).update(request, pk, partial=True)

    def list(self, request):
        signs = self.request.query_params.getlist('sign', [])
        for sign in signs:
                self.queryset = self.queryset.filter(signs__name=sign)
        term = self.request.query_params.get('search', None)
        if term is not None:
            self.queryset = self.queryset.filter(title__icontains=term)
        count = self.request.query_params.get('count', None)
        start = self.request.query_params.get('start', None)
        if start:
            try:
                start = int(start)
                if start < 0:
                    start = 0
            except Exception:
                pass
        if count:
            try:
                count = int(count)
                if count < 0:
                    count = 0
                if start:
                    count = start + count
            except Exception:
                pass
        self.queryset = self.queryset.order_by('-modified')[start:count]
        return super(CampViewSet, self).list(request)
