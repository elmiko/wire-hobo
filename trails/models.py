# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import uuid

from django.db import models
from django.utils import timezone


class SignModel(models.Model):
    """wire hobo signs

    Signs are the identifiers that tell a wise hobo about a trail and
    what it may contain.
    """
    id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(unique=True, max_length=100)

    class Meta:
        ordering = ('name',)


class CampModel(models.Model):
    """wire hobo camps

    Camps are the individual stops along the way, they contain some
    signs which tell about the location, and the location info.
    """
    id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    created = models.DateTimeField(default=timezone.now)
    modified = models.DateTimeField(auto_now=True)
    signs = models.ManyToManyField(SignModel, blank=True)
    title = models.CharField(max_length=255)
    url = models.URLField(unique=True)

    class Meta:
        ordering = ('created',)
