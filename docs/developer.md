# how to hack on the wire hobo

Using a [Pipenv](https://pipenv.pypa.io/en/latest/) is the easiest way to get started hacking
on the wire hobo code. These instructions will get you setup with an
environment and a fresh database.

## setup devel environment

```
pipenv install --dev
```

## copy example database and run debug server

```
cp ./examples/example.db.sqlite3 ./db.sqlite3
pipenv run debugserver
```

## run tests

```
pipenv run tests
```

## run django commands

```
pipenv run ./manage.py <args>
```
