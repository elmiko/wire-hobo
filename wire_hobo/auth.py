"""Custom authentication stuff

Classes:
APIKeyHeaderAuthentication -- authenticates a request based on an a
                              shared token key.
"""
import hashlib

from django.conf import settings
from django.contrib.auth import models
from rest_framework import authentication
from rest_framework import exceptions


class APIUser(models.AnonymousUser):
    def is_authenticated(self):
        return True


class APIKeyHeaderAuthentication(authentication.BaseAuthentication):
    """Authenticate a request

    The request must contain the header `wire-hobo-apikey` which
    contains the key for gaining access to the trails.
    """
    def authenticate(self, request):
        apikey = request.META.get('HTTP_WIRE_HOBO_APIKEY')
        if apikey is None:
            return None
        sha = hashlib.sha512()
        sha.update(apikey.encode('utf8'))
        hd = sha.hexdigest()
        if hd != settings.API_KEY:
            raise exceptions.AuthenticationFailed('Unknown API key')
        return (APIUser(), None)


class IndexHTMLAuthentication(authentication.BaseAuthentication):
    """Allow access to the index page

    The index page is meant to be public for html requests, this
    class simply allows access.
    """
    def authenticate(self, request):
        accept = request.META.get('HTTP_ACCEPT', '')
        if request.path == '/' and 'text/html' in accept:
            return (APIUser(), None)
        if request.path == '/feed':
            return (APIUser(), None)
        return None

