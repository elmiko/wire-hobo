# -*- coding: utf-8 -*-
import json

from django.http import HttpResponseNotFound
from rest_framework.response import Response
from rest_framework import viewsets
from rest_framework.views import exception_handler as parent_handler


def exception_handler(exc, context):
    res = parent_handler(exc, context)

    data = {
        'error': {
            'detail': str(exc),
        },
    }

    res.data = data
    return res


def not_found_handler(request, exception=None):
    data = {
        'error': {
            'detail': 'Not found',
        },
    }
    return HttpResponseNotFound(json.dumps(data), content_type='application/json')
