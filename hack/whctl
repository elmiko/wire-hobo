#!/bin/env python3
# Copyright (C) 2020  michael mccune
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
'''command line interface for wire-hobo servers'''
import argparse
import cmd
from collections import namedtuple
import configparser
from copy import deepcopy
from http import client
import json
import logging
import os
from os import path
import shlex
import sys


# Server Connection Stuff
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
class Connection():
    def __init__(self, url, key):
        logging.debug(f'creating connection for {url}')
        transport, host = url.split('://')
        connectioncls = {
            'http': client.HTTPConnection,
            'https': client.HTTPSConnection,
        }.get(transport)
        if connectioncls is None:
            raise Exception(f'unknown transport type "{transport}"')
        self.connection = connectioncls(host)
        self.headers = {
            'wire-hobo-apikey': key,
            'Accept': 'application/json',
        }

    def request(self, method, url, body=None):
        logging.debug(f'request {method} {url}')
        headers = deepcopy(self.headers)
        if body:
            headers.update({'Content-Type': 'application/json'})
        self.connection.request(method, url, headers=headers, body=body)
        res = self.connection.getresponse()
        payload = res.read()
        self.connection.close()
        if res.status < 200 or res.status > 299:
            logging.debug(f'{payload.decode("utf8")}')
            raise Exception(f'unexpected status {res.status} on {method} request to {url}')
        return payload


def utf8(func):
    def wrapper(*args, **kwargs):
        r = func(*args, **kwargs)
        r.decode('utf8')
        return r
    return wrapper


@utf8
def create_camp(connection, title, url, signs=None):
    logging.debug(f'creating camp, title {title}, url {url}, signs {signs}')
    payload = json.dumps({
        'title': title,
        'url': url,
        'signs': signs if signs else []})
    return connection.request('POST', '/camps', body=payload)


@utf8
def delete_camp(connection, campid):
    logging.debug(f'deleting camp, id {campid}')
    return connection.request('DELETE', f'/camps/{campid}')


@utf8
def delete_sign(connection, signid):
    logging.debug(f'deleting sign, id {signid}')
    return connection.request('DELETE', f'/signs/{signid}')


@utf8
def read_camps(connection, count=None, signs=None, search=None):
    logging.debug(f'reading camps, count {count}, signs {signs}, search {search}')
    params = []
    if count:
        params.append(f'count={count}')
    if signs:
        for sign in signs:
            params.append(f'signs={sign}')
    if search:
        params.append(f'search={search}')
    url = '/camps'
    for i, param in enumerate(params):
        if i == 0:
            url += '?'
        else:
            url += '&'
        url += param
    logging.debug(url)
    return connection.request('GET', url)


@utf8
def read_server(connection):
    logging.debug('reading server information')
    return connection.request('GET', '/')


@utf8
def read_signs(connection, count=None):
    logging.debug(f'reading signs, count {count}')
    url = '/signs'
    if count:
        url += f'?count={count}'
    return connection.request('GET', url)


@utf8
def update_camp(connection, campid, url=None, title=None, signs=None):
    logging.debug(f'updating camp, id {campid}, url {url}, title {title}, signs {signs}')
    body = {}
    if url:
        body['url'] = url
    if title:
        body['title'] = title
    if signs:
        body['signs'] = signs
    payload = json.dumps(body)
    return connection.request('PUT', f'/camps/{campid}', body=payload)


@utf8
def update_sign(connection, signid, name):
    logging.debug(f'updating sign, id {signid}, name {name}')
    payload = json.dumps({'name': name})
    return connection.request('PUT', f'/signs/{signid}', body=payload)


# Parser Stuff
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
class MainArgParser(argparse.ArgumentParser):
    def __init__(self):
        super().__init__(add_help=False,description='interact with a wire-hobo server')
        default_config = path.join(os.environ['HOME'], '.config/wirehobo/wirehobo.conf')
        self.add_argument(
                '-d', '--debug', action='store_true',
                help='enable debug mode')
        self.add_argument(
                '-c', '--config', default=default_config,
                help=f'path of config file, defaults to {default_config}')
        self.add_argument(
                'commands', nargs=argparse.REMAINDER,
                help='commands and arguments to control interaction')


class CreateCampArgParser(argparse.ArgumentParser):
    def __init__(self):
        super().__init__(
                add_help=False,
                description=CreateCmd.do_camp.__doc__,
                usage='whcli create camp --url URL --title TITLE [--sign SIGN]')
        self.add_argument(
                '--url', required=True,
                help='url of camp')
        self.add_argument(
                '--title', required=True,
                help='title for camp')
        self.add_argument(
                '--sign', action='append',
                help='sign name to add on the camp, can be supplied multiple times')


class DeleteCampArgParser(argparse.ArgumentParser):
    def __init__(self):
        super().__init__(
                add_help=False,
                description=DeleteCmd.do_camp.__doc__,
                usage='whcli delete camp --id ID')
        self.add_argument(
                '--id', required=True,
                help='id of the camp to delete')


class DeleteSignArgParser(argparse.ArgumentParser):
    def __init__(self):
        super().__init__(
                add_help=False,
                description=DeleteCmd.do_sign.__doc__,
                usage='whcli delete sign --id ID')
        self.add_argument(
                '--id', required=True,
                help='id of the sign to delete')


class ReadCampsArgParser(argparse.ArgumentParser):
    def __init__(self):
        super().__init__(
                add_help=False,
                description=ReadCmd.do_camps.__doc__,
                usage='whcli read camps [--count COUNT] [--sign SIGN] [--search SEARCH]')
        self.add_argument(
                '--count', type=int,
                help='count of camps to return')
        self.add_argument(
                '--sign', action='append',
                help='sign name to use as a filter, can be supplied multiple times')
        self.add_argument(
                '--search',
                help='string to use as a match filter for camp titles')


class ReadSignsArgParser(argparse.ArgumentParser):
    def __init__(self):
        super().__init__(
                add_help=False,
                description=ReadCmd.do_camps.__doc__,
                usage='whcli read signs [--count COUNT]')
        self.add_argument(
                '--count', type=int,
                help='count of camps to return')


class UpdateCampArgParser(argparse.ArgumentParser):
    def __init__(self):
        super().__init__(
                add_help=False,
                description=UpdateCmd.do_camp.__doc__,
                usage='whcli update camp --id ID [--url URL] [--title TITLE] [--sign SIGN]')
        self.add_argument(
                '--id', required=True,
                help='id of the camp to update')
        self.add_argument(
                '--url',
                help='url of camp')
        self.add_argument(
                '--title',
                help='title for camp')
        self.add_argument(
                '--sign', action='append',
                help='sign name to add on the camp, can be supplied multiple times')


class UpdateSignArgParser(argparse.ArgumentParser):
    def __init__(self):
        super().__init__(
                add_help=False,
                description=UpdateCmd.do_sign.__doc__,
                usage='whcli update sign --id ID [--name NAME]')
        self.add_argument(
                '--id', required=True,
                help='id of the sign to update')
        self.add_argument(
                '--name', required=True,
                help='name of sign')


# Command Stuff
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
class BaseCmd(cmd.Cmd):
    def __init__(self, connection):
        super().__init__()
        self.connection = connection


class ActionCmd(BaseCmd):
    def do_help(self, arg):
        '''print all the help'''
        p = MainArgParser()
        p.print_help()
        super().do_help(arg)

    def do_create(self, arg):
        '''create a resource'''
        return CreateCmd(self.connection).onecmd(arg)

    def do_delete(self, arg):
        '''delete a resource'''
        return DeleteCmd(self.connection).onecmd(arg)

    def do_read(self, arg):
        '''read a resource'''
        return ReadCmd(self.connection).onecmd(arg)

    def do_update(self, arg):
        '''update a resource'''
        return UpdateCmd(self.connection).onecmd(arg)

    def emptyline(self):
        print('No command specified')
        self.do_help(None)


class CreateCmd(BaseCmd):
    def help_camp(self):
        p = CreateCampArgParser()
        p.print_help()

    def do_camp(self, arg):
        '''create a new camp'''
        parser = CreateCampArgParser()
        args = parser.parse_args(shlex.split(arg))
        return create_camp(self.connection, args.title, args.url, args.sign)


class DeleteCmd(BaseCmd):
    def help_camp(self):
        p = DeleteCampArgParser()
        p.print_help()

    def help_sign(self):
        p = DeleteSignArgParser()
        p.print_help()

    def do_camp(self, arg):
        '''delete a camp'''
        parser = DeleteCampArgParser()
        args = parser.parse_args(shlex.split(arg))
        return delete_camp(self.connection, args.id)

    def do_sign(self, arg):
        '''delete a sign'''
        parser = DeleteSignArgParser()
        args = parser.parse_args(shlex.split(arg))
        return delete_sign(self.connection, args.id)


class ReadCmd(BaseCmd):
    def help_camps(self):
        p = ReadCampsArgParser()
        p.print_help()

    def do_help(self, arg):
        '''print all the help'''
        p = ReadCampsArgParser()
        p.print_help()
        super().do_help(arg)

    def do_camps(self, arg):
        '''read some camps'''
        parser = ReadCampsArgParser()
        args = parser.parse_args(shlex.split(arg))
        return read_camps(self.connection, args.count, args.sign, args.search)

    def do_server(self, arg):
        '''read the server information'''
        return read_server(self.connection)

    def help_signs(self):
        p = ReadSignsArgParser()
        p.print_help()

    def do_signs(self, arg):
        '''read some signs'''
        parser = ReadSignsArgParser()
        args = parser.parse_args(shlex.split(arg))
        return read_signs(self.connection, args.count)

    def emptyline(self):
        print('No resource specified')
        self.do_help(None)


class UpdateCmd(BaseCmd):
    def help_camp(self):
        p = UpdateCampArgParser()
        p.print_help()

    def do_camp(self, arg):
        '''update a camp'''
        parser = UpdateCampArgParser()
        args = parser.parse_args(shlex.split(arg))
        return update_camp(self.connection, args.id, args.url, args.title, args.sign)

    def do_sign(self, arg):
        '''update a sign'''
        parser = UpdateSignArgParser()
        args = parser.parse_args(shlex.split(arg))
        return update_sign(self.connection, args.id, args.name)


# Main
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Config = namedtuple('Config', ['url', 'key'])


def get_config(filename):
    if not path.exists(filename):
        raise Exception(f'Cannot find configuration file "{filename}"')
    parser = configparser.ConfigParser()
    parser.read(filename)
    if 'server' not in parser:
        raise Exception('Cannot find "server" section in configuration file')
    if not parser['server'].get('url') and not url:
        raise Exception('Cannot find "url" key in configuration file')
    if not parser['server'].get('key') and not key:
        raise Exception('Cannot find "key" key in configuration file')
    return Config(parser['server'].get('url'),parser['server'].get('key'))


def main(args):
    config = get_config(args.config)
    logging.debug(f'server url {config.url}')

    connection = Connection(config.url, config.key)

    logging.debug(f'processing commands "{args.commands}"')
    if res := ActionCmd(connection).onecmd(' '.join(args.commands)):
        j = json.loads(res)
        print(json.dumps(j, indent=2, sort_keys=True))


if __name__ == '__main__':
    parser = MainArgParser()
    args = parser.parse_args()

    loglevel = logging.WARNING
    if args.debug:
        loglevel = logging.DEBUG
    logging.basicConfig(level=loglevel)

    try:
        main(args)
    except Exception as ex:
        if args.debug:
            raise ex
        logging.error(str(ex))
        sys.exit(1)
