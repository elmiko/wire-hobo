#!/bin/env python3
""" wire hobo server copy utility

a quick and dirty script to copy all camps and signs from one server to
another. to start make an ini style config file with the following format:

    [source]
    url = https://source.server.com
    key = source-api-key
    [target]
    url = https://target.server.com
    key = target-api-key

"""
import argparse
import collections
import configparser
import logging

import requests


WHConfig = collections.namedtuple(
        'WHConfig',
        ['source_url', 'source_key', 'target_url', 'target_key'])


def get_camp_dict(camp):
    return {
        'title': camp.get('title'),
        'url': camp.get('url'),
        'signs': camp.get('signs'),
        'created': camp.get('created')
    }


def main(config):
    logging.info('starting download')
    url = f'{config.source_url}/camps'
    headers = {
        'wire-hobo-apikey': config.source_key,
        'Content-Type': 'application/json'
    }
    res = requests.get(url, headers=headers)
    if res.status_code != 200:
        logging.error(
            f'received {res.status_code} status from source server')
        logging.error(f' {res.text}')
        raise Exception('Unable to read from source server')

    logging.info(f'received {len(res.json())} camps')
    logging.info('starting upload')
    count = 0
    url = f'{config.target_url}/camps'
    headers = {'wire-hobo-apikey': config.target_key}
    for camp in res.json():
        payload = get_camp_dict(camp)
        res = requests.post(url, headers=headers, json=payload)
        if res.status_code != 201:
            logging.error(
                f'received {res.status_code} status from target server')
            logging.error(f' {res.text}')
            raise Exception('Unable to write to target server')
        count += 1
    logging.info(f'finished upload, wrote {count} camps')


def get_config(filename):
    logging.info(f'opening config file {filename}')
    config = configparser.ConfigParser()
    config.read(filename)
    ret = WHConfig(
        config['source']['url'],
        config['source']['key'],
        config['target']['url'],
        config['target']['key'])
    return ret


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    logging.info('wire hobo server copy')
    parser = argparse.ArgumentParser(description='copy a wire-hobo server')
    parser.add_argument('config', help='config file path')
    args = parser.parse_args()
    config = get_config(args.config)
    main(config)
