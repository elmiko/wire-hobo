#!/usr/bin/env python
import hashlib
import sys

def main():
    try:
        value = sys.argv[1]
    except Exception:
        sys.exit(-1)

    sha = hashlib.sha512()
    sha.update(value.encode('utf8'))
    print(sha.hexdigest())
    sys.exit(0)


if __name__ == '__main__':
    main()
