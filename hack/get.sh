#!/bin/sh
set -e

server=${WIREHOBO_SERVER:-"http://localhost:8000"}
action=${1:-"server"}

case "$action" in
    "camp"     ) uri="/camps/$2";;
    "camps"     ) uri="/camps";;
    "sign"      ) uri="/signs/$2";;
    "signs"     ) uri="/signs";;
    *           ) uri="/";;
esac

curl -H "wire-hobo-apikey: $WIREHOBO_APIKEY" \
     -H "accept: application/json" \
     -w "%{stdout}\n" \
     "${server}${uri}"

