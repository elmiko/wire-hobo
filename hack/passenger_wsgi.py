import sys, os
INTERP = "/path/to/your/python"
#INTERP is present twice so that the new python interpreter knows the actual executable path
if sys.executable != INTERP: os.execl(INTERP, INTERP, *sys.argv)

cwd = os.getcwd()
sys.path.append(cwd)
sys.path.append(cwd + '/server')  #You must add your project here

sys.path.insert(0,cwd+'/env/bin')

os.environ['DJANGO_SETTINGS_MODULE'] = "wire_hobo.settings_live"
from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
