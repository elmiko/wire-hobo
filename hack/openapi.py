"""openapi helpers

This is a quick and dirty script to make generating an OpenAPI schema
for the wire hobo much easier.

To use this module, recommend the following:

    $ ./manage.py shell --pythonpath ../hack
    >>> import openapi
    >>> openapi.save_api('some_filename')
"""

import json

from rest_framework.schemas import SchemaGenerator
from rest_framework_swagger.renderers import OpenAPICodec


def get_schema():
    schema = SchemaGenerator().get_schema()
    extras = {
        'securityDefinitions': {
            'api_key': {
                'type': 'apiKey',
                'name': 'wire-hobo-apikey',
                'in': 'header'
                }
            }
        }
    return OpenAPICodec().encode(schema, extra=extras)


def save_api(filename):
    out = open(filename, 'w')
    schema = get_schema()
    data = json.loads(schema)
    out.write(json.dumps(data, indent=2))
    out.close()
